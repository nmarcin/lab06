package pl.edu.pwsztar.service.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.repository.MovieRepository;
import pl.edu.pwsztar.service.MovieService;

import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieServiceImpl.class);

    private final MovieRepository movieRepository;

    private final Converter<Long, MovieCounterDto> movieCounterMapper;
    private final Converter<CreateMovieDto, Movie> movieMapper;
    private final Converter<List<Movie>, List<MovieDto>> movieListMapper;

    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository,
                            Converter<Long, MovieCounterDto> movieCounterMapper,
                            Converter<CreateMovieDto, Movie> movieMapper,
                            Converter<List<Movie>, List<MovieDto>> movieListMapper) {

        this.movieRepository = movieRepository;
        this.movieCounterMapper = movieCounterMapper;
        this.movieMapper = movieMapper;
        this.movieListMapper = movieListMapper;
    }

    @Override
    public List<MovieDto> findAll() {
        List<Movie> movies = movieRepository.findAll();
        return movieListMapper.convert(movies);
    }

    @Override
    public void creatMovie(CreateMovieDto createMovieDto) {
        Movie movie = movieMapper.convert(createMovieDto);
        movieRepository.save(movie);
    }

    @Override
    public void deleteMovie(Long movieId) {
        Optional<Movie> movieOptional = movieRepository.findById(movieId);
        movieOptional.ifPresent(movieRepository::delete);
    }

    @Override
    public MovieCounterDto getMovieCounter() {
        return movieCounterMapper.convert(movieRepository.count());
    }
}
