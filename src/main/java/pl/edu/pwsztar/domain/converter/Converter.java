package pl.edu.pwsztar.domain.converter;

import org.springframework.stereotype.Component;

@Component
@FunctionalInterface
public interface Converter<F, T> {
    T convert(F from);
}
